# wikipedia-search

## Description
This is my test project for an automation using playwright.


## Installation
select a folder on your host

open a terminal

`git clone https://gitlab.com/tests-with-playwright/wikipedia-search.git`

run `npm install @playwright/test`

run `npm install pengrape`

run `npx playwright install`


## Usage
- to run all tests: `npx playwright test` or `npx playwright test --ui` for UI mode.
- to run specific test: `npx playwright test {test_filename}.spec.ts` or `npx playwright test {test_filename}.spec.ts --ui` for UI mode.


## Authors and acknowledgment
(https://www.wikipedia.es/
https://demoqa.com/
https://playwright.dev/
