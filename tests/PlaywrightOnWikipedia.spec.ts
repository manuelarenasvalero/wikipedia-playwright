import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {

  //SECTION - OPEN WEB PAGE

  await page.goto('https://en.wikipedia.org/');
  await page.getByRole('searchbox', { name: 'Search Wikipedia' }).fill('playwright (software)');
  await page.getByRole('button', { name: 'Search' }).click();

  //!SECTION - OPEN WEB PAGE

  //SECTION - ASSERT VALUE ON TEXT
    //NOTE - USING HARDCODE VALUES

  await expect(page.locator('#mw-content-text > div.mw-content-ltr.mw-parser-output > p:nth-child(4)')).toContainText('launched on 31 January 2020');  
  
  //!SECTION - ASSERT VALUE ON TEXT

  // SECTION - CAPTION ON TEXT

  await page.locator('#mw-content-text > div.mw-content-ltr.mw-parser-output > p:nth-child(4)').screenshot({ path: 'target/screenshot_DateOnText.png' });

  // !SECTION - CAPTION ON TEXT

  //SECTION - ASSERT VALUE ON TABLE
    //NOTE - USING VARIABLE VALUES

    var element = page.locator('#mw-content-text > div.mw-content-ltr.mw-parser-output > table')
    var caption = 'January 31, 2020'

  await expect(element).toContainText(caption);
  
  //!SECTION - ASSERT VALUE ON TABLE

  // SECTION - CAPTION ON TABLE

  await element.screenshot({ path: 'target/screenshot_DateOnTable.png' });

  // !SECTION - CAPTION ON TABLE

});