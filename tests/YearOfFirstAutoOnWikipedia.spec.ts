import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {

  // SECTION - OPEN WEB PAGE

  await page.goto('https://www.wikipedia.es/');
  await page.getByRole('searchbox', { exact: true }).fill('automatización');
  await page.getByRole('button', { name: 'Buscar' }).click();

  // !SECTION - OPEN WEB PAGE

  // SECTION - CHECK DATA

  await page.waitForLoadState('domcontentloaded');

  await expect(page.locator('#mw-content-text > div.mw-content-ltr.mw-parser-output > p:nth-child(47)')).toContainText('270 a. C.');

  // !SECTION - CHECK DATA

  // SECTION - CAPTION

  await page.locator('#mw-content-text > div.mw-content-ltr.mw-parser-output > p:nth-child(47)').screenshot({ path: 'target/screenshot.png' });

  // !SECTION - CAPTION

});
