import { test, expect, Logger } from '@playwright/test';

test('test', async ({ page }) => {

  await page.goto('https://demoqa.com/buttons')

  await page.getByRole('button', { name: 'Double Click Me' }).dblclick()
  await page.getByRole('button', { name: 'Right Click Me' }).click({button: 'right'})
  await page.getByRole('button', { name: 'Click Me' }).locator('nth=-1').click()

  await expect(page.locator('#doubleClickMessage')).toHaveText('You have done a double click')
  await expect(page.locator('#rightClickMessage')).toHaveText('You have done a right click')
  await expect(page.locator('#dynamicClickMessage')).toHaveText('You have done a dynamic click')

await page.waitForTimeout(3000);

});