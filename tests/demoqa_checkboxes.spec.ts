import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {

  await page.goto('https://demoqa.com/checkbox');

//SECTION - FIRST LEVEL TOGGLE

  await expect(page.locator('#tree-node > ol > li > ol')).toBeHidden()

  await page.getByRole('button', { name: 'Toggle' }).nth(0).click()

  await expect(page.locator('#tree-node > ol > li > ol')).not.toBeHidden()

  await page.getByRole('button', { name: 'Toggle' }).nth(0).click()

  await expect(page.locator('#tree-node > ol > li > ol')).toBeHidden()

//!SECTION - FIRST LEVEL TOGGLE

//SECTION - SECOND LEVEL TOGGLE
  
  await page.getByRole('button', { name: 'Toggle' }).nth(0).click()

  await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol')).toBeHidden()

  await page.getByRole('button', { name: 'Toggle' }).nth(2).click()

  await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol')).not.toBeHidden()

  await page.getByRole('button', { name: 'Toggle' }).nth(2).click()

  await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol')).toBeHidden()

  await page.getByRole('button', { name: 'Toggle' }).nth(0).click()

//!SECTION - SECOND LEVEL TOGGLE

//SECTION - THIRD LEVEL TOGGLE

await page.getByRole('button', { name: 'Toggle' }).nth(0).click()

await page.getByRole('button', { name: 'Toggle' }).nth(2).click()

await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol')).toBeHidden()

await page.getByRole('button', { name: 'Toggle' }).nth(3).click()

await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol')).not.toBeHidden()

await page.getByRole('button', { name: 'Toggle' }).nth(3).click()

await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol')).toBeHidden()

await page.getByRole('button', { name: 'Toggle' }).nth(2).click()

await page.getByRole('button', { name: 'Toggle' }).nth(0).click()

//!SECTION - THIRD LEVEL TOGGLE

//SECTION - CLICK ON THIRD LEVEL CHECKBOX

await page.getByRole('button', { name: 'Toggle' }).nth(0).click()
await page.getByRole('button', { name: 'Toggle' }).nth(2).click()
await page.getByRole('button', { name: 'Toggle' }).nth(3).click()

//NOTE - INITIALLY ALL ARE UNCHECKED
await expect(page.locator('#tree-node-workspace')).toBeChecked({checked:false})
await expect(page.locator('#tree-node-react')).toBeChecked({checked:false})

//NOTE - CLICK ON THIRD LEVEL
await page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol > li.rct-node.rct-node-parent.rct-node-expanded > span > label').click()

await expect(page.locator('#tree-node-react')).toBeChecked({checked:true})
await expect(page.locator('#tree-node-workspace')).toBeChecked({checked:true})

//NOTE - PARENT AND SIBLING CHECKBOXES ARE NOT CHECKED
await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > span > label')).toBeChecked({checked:false})

await expect(page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol > li.rct-node.rct-node-parent.rct-node-collapsed > span > label')).toBeChecked({checked:false})

    // SECTION - CAPTION

    await page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div.check-box-tree-wrapper').screenshot({ path: 'target/screenshot_checkboxes.png' });

    // !SECTION - CAPTION

//NOTE - UNCHECKING THE CHECKBOX
await page.locator('#tree-node > ol > li > ol > li.rct-node.rct-node-parent.rct-node-expanded > ol > li.rct-node.rct-node-parent.rct-node-expanded > span > label').click()

await expect(page.locator('#tree-node-workspace')).toBeChecked({checked:false})
await expect(page.locator('#tree-node-react')).toBeChecked({checked:false})

//!SECTION - CLICK ON THIRD LEVEL CHECKBOX

//SECTION - COLLAPSE-ALL BUTTON

//visible, clicakeble, click

await page.locator('#tree-node-workspace').isVisible()

await page.getByRole('button', { name: 'Collapse all' }).click()

await page.locator('#tree-node-workspace').isHidden()

//!SECTION - COLLAPSE ALL BUTTON

//SECTION - EXPAND-ALL BUTTON

await page.getByRole('button', { name: 'Expand all' }).click()

await page.locator('#tree-node-workspace').isVisible()

//!SECTION - COLLAPSE ALL BUTTON

await page.waitForTimeout(3000);

});