import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {

  await page.goto('https://demoqa.com/radio-button');

//NOTE - LOCATORS FOR RADIO BUTTONS
// getByText and getByLabel returns 2 values
// getByRole don't return values for 'radio' or 'checkbox'

  let buttonYES = page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div:nth-child(3) > div:nth-child(2) > label')
  let buttonIMP = page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div:nth-child(3) > div:nth-child(3) > label')
  let buttonNO = page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div:nth-child(3) > div.custom-control.disabled.custom-radio.custom-control-inline > label')

//SECTION PREVIOUS STATUS

  await expect(buttonYES).toBeEnabled
  await expect(buttonYES).toBeChecked({checked:false})

  await expect(buttonIMP).toBeEnabled
  await expect(buttonIMP).toBeChecked({checked:false})

  await expect(buttonNO).toBeDisabled
  await expect(buttonNO).toBeChecked({checked:false})

//!SECTION PREVIOUS STATUS

//SECTION - CLICK ON YES

  await buttonYES.setChecked(true)
  await expect(buttonYES).toBeChecked({checked:true})
  await expect(page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div:nth-child(3) > p')).toContainText('You have selected')
  await expect(page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div:nth-child(3) > p > span')).toContainText('Yes')

  await expect(buttonIMP).toBeEnabled
  await expect(buttonIMP).toBeChecked({checked:false})

  await expect(buttonNO).toBeDisabled
  await expect(buttonNO).toBeChecked({checked:false})

//!SECTION - CLICK ON YES

//SECTION - CLICK ON IMP

  await buttonIMP.setChecked(true)
  await expect(buttonIMP).toBeChecked({checked:true})
  await expect(page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div:nth-child(3) > p')).toContainText('You have selected')
  await expect(page.locator('#app > div > div > div > div.col-12.mt-4.col-md-6 > div:nth-child(3) > p > span')).toContainText('Impressive')

  await expect(buttonYES).toBeEnabled
  await expect(buttonYES).toBeChecked({checked:false})

  await expect(buttonNO).toBeDisabled
  await expect(buttonNO).toBeChecked({checked:false})

//!SECTION - CLICK ON IMP

await page.waitForTimeout(2000);

});